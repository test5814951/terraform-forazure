# Terraform Infrastructure as Code for Azure

This repository contains Terraform code for provisioning and managing resources in Microsoft Azure using Infrastructure as Code (IaC) principles.

## Prerequisites

Before you begin, ensure you have the following tools installed:

- [Terraform](https://www.terraform.io/downloads.html)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

Make sure you have the necessary credentials for Azure, including a service principal with appropriate permissions.

## Getting Started

1. Clone this repository:

```bash
git clone https://gitlab.com/test5814951/terraform-forazure.git
cd terraform-forazure
```

2. Initialize Terraform:
```bash
terraform init
```

## Folder Structure
The folder structure is organized as follows:
```bash
.
|-- modules/
|   |-- key_vault/
|   |-- aks/
|   |-- cdn/
|   |-- app_gateway/
|   |-- virtual_network/
|   |-- storage_account/
|   |-- postgresql/
|   |-- load_balancer/
|   |-- defender/
|-- environments/
|   |-- dev/
|   |-- uat/
|   |-- preprod/
|   |-- prod/
|-- shared.tfvars
|-- backend.tf
|-- provider.tf
|-- variables.tf
|-- main.tf
|-- README.md
```

**modules**: Contains reusable Terraform modules for each Azure resource.

**environments**: Contains environment-specific configurations.

**shared.tfvars**: Common variables shared across environments.

**backend.tf**: Configurations for the Terraform backend (state storage).

**provider.tf:** Azure provider configurations.

**variables.tf**: Global variables used across the Terraform configuration.

**main.tf**: Main Terraform configuration entry point.

**README.md**: Project documentation.


**Modules**

**key_vault**: Azure Key Vault module.

**aks**: Azure Kubernetes Service (AKS) module.

**cdn**: Content Delivery Network (CDN) module.

**app_gateway**: Azure Application Gateway module.

**virtual_network**: Azure Virtual Network module.

**storage_account**: Azure Storage Account module.

**postgresql**: Azure Database for PostgreSQL module.

**load_balancer**: Azure Load Balancer module.

**defender**: Microsoft Defender module.

## Environments

The environments folder contains subdirectories for each environment:

**dev**: Development environment.

**uat**: User Acceptance Testing (UAT) environment.

**preprod**: Pre-production environment.

**prod**: Production environment.

## Running Terraform
To create resources for a specific environment, follow these steps:

1. Navigate to the desired environment folder:
 ```bash
terraform init -var-file=environments/dev/dev.tfvars
terraform plan -var-file=environments/dev/dev.tfvars
terraform apply -var-file=environments/dev/dev.tfvars
```


