variable "tenant_id" {
  description = "Azure AD Tenant ID"
}

variable "client_id" {
  description = "Azure AD Client ID"
}

variable "client_secret" {
  description = "Azure AD Client Secret"
}

variable "subscription_id" {
  description = "Azure Subscription ID"
}

variable "location" {
  description = "Azure Region"
  default     = "East US"  # Set a default value or remove this line if not applicable
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "virtual_network_name" {
  description = "Name of the Azure Virtual Network"
}

variable "subnet_name" {
  description = "Name of the Azure Subnet"
}

variable "admin_username" {
  description = "Administrator username for VMs"
}

variable "admin_password" {
  description = "Administrator password for VMs"
  sensitive   = true  # Marking it as sensitive hides the value in the Terraform CLI output
}

variable "acr_name" {
  description = "Name of the Azure Container Registry"
}

variable "vm_size" {
  description = "Size of the Virtual Machine"
  default     = "Standard_DS2_v2"
}

variable "os_image_publisher" {
  description = "Publisher of the OS image for VMs"
  default     = "Canonical"
}

variable "os_image_offer" {
  description = "Offer of the OS image for VMs"
  default     = "UbuntuServer"
}

variable "os_image_sku" {
  description = "SKU of the OS image for VMs"
  default     = "18.04-LTS"
}

variable "os_image_version" {
  description = "Version of the OS image for VMs"
  default     = "latest"
}

variable "vnet_address_space" {
  description = "Address space for the Azure Virtual Network"
  type        = list(string)
  default     = ["10.0.0.0/16"]
}

variable "subnet_address_space" {
  description = "Address space for the Azure Subnet"
  type        = list(string)
  default     = ["10.0.1.0/24"]
}

variable "key_vault_name" {
  description = "Name of the Azure Key Vault"
}

variable "aks_cluster_name" {
  description = "Name of the AKS Cluster"
}

variable "cdn_profile_name" {
  description = "Name of the CDN Profile"
}

variable "app_gateway_name" {
  description = "Name of the Azure Application Gateway"
}

variable "storage_account_name" {
  description = "Name of the Azure Storage Account"
}

variable "postgresql_server_name" {
  description = "Name of the PostgreSQL Server"
}

variable "load_balancer_name" {
  description = "Name of the Azure Load Balancer"
}

variable "defender_plan_name" {
  description = "Name of the Microsoft Defender Plan"
}

# Add other common variables needed for various resources
