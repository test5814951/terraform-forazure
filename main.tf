provider "azurerm" {
  features = {}
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  subscription_id = var.subscription_id
}

# Azure Key Vault
module "key_vault" {
  source              = "./modules/key_vault"
  key_vault_name      = var.key_vault_name
  resource_group_name = var.resource_group_name
  location            = var.location
}

# Azure Kubernetes Service (AKS)
module "aks" {
  source              = "./modules/aks"
  aks_cluster_name    = var.aks_cluster_name
  resource_group_name = var.resource_group_name
  location            = var.location
}

# Content Delivery Network (CDN)
module "cdn" {
  source              = "./modules/cdn"
  cdn_profile_name    = var.cdn_profile_name
  resource_group_name = var.resource_group_name
  location            = var.location
}

# Azure Application Gateway
module "app_gateway" {
  source              = "./modules/app_gateway"
  app_gateway_name    = var.app_gateway_name
  resource_group_name = var.resource_group_name
  location            = var.location
}

# Azure Virtual Network
module "virtual_network" {
  source              = "./modules/virtual_network"
  virtual_network_name = var.virtual_network_name
  resource_group_name  = var.resource_group_name
  location             = var.location
  address_space        = var.vnet_address_space
  subnet_name          = var.subnet_name
  subnet_address_space = var.subnet_address_space
}

# Azure Storage Account
module "storage_account" {
  source              = "./modules/storage_account"
  storage_account_name = var.storage_account_name
  resource_group_name = var.resource_group_name
  location            = var.location
}

# Azure PostgreSQL Server
module "postgresql" {
  source               = "./modules/postgresql"
  postgresql_server_name = var.postgresql_server_name
  resource_group_name   = var.resource_group_name
  location              = var.location
}

# Azure Load Balancer
module "load_balancer" {
  source               = "./modules/load_balancer"
  load_balancer_name   = var.load_balancer_name
  resource_group_name  = var.resource_group_name
  location             = var.location
}

# Microsoft Defender
module "defender" {
  source             = "./modules/defender"
  defender_plan_name = var.defender_plan_name
}

# Azure Container Registry (ACR)
module "acr" {
  source               = "./modules/acr"
  acr_name             = var.acr_name
  resource_group_name  = var.resource_group_name
  location             = var.location
}

# Virtual Machines
module "vms" {
  source               = "./modules/vms"
  admin_username       = var.admin_username
  admin_password       = var.admin_password
  vm_size              = var.vm_size
  os_image_publisher   = var.os_image_publisher
  os_image_offer       = var.os_image_offer
  os_image_sku         = var.os_image_sku
  os_image_version     = var.os_image_version
  resource_group_name  = var.resource_group_name
  location             = var.location
  virtual_network_name = var.virtual_network_name
}

