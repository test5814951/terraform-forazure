# shared.tfvars

# Azure AD
tenant_id       = "your-shared-tenant-id"
client_id       = "your-shared-client-id"
client_secret   = "your-shared-client-secret"
subscription_id = "your-shared-subscription-id"

# Common
location              = "East US"
resource_group_name   = "shared-rg"
admin_username        = "adminuser"
admin_password        = "SharedAdminPassword123!"  # Use secure methods for handling passwords in production

# ACR
acr_name = "shared-acr"

# VMs
vm_size              = "Standard_DS2_v2"
os_image_publisher   = "Canonical"
os_image_offer       = "UbuntuServer"
os_image_sku         = "18.04-LTS"
os_image_version     = "latest"
