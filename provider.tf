terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.83.0"  # Specify the desired version of the AzureRM provider
    }
  }
}
