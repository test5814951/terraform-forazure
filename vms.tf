# vms.tf

# Resource block for the first virtual machine
resource "azurerm_virtual_machine" "example_vm_1" {
  name                  = "example-vm-1"
  resource_group_name   = "<resource-group-name>"
  location              = "<azure-region>"
  availability_set_id   = "<availability-set-id>" # Optional, if using an availability set

  vm_size               = "Standard_DS2_v2"
  network_interface_ids = ["<network-interface-id>"]

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "example-vm-1"
    admin_username = "adminuser"
    ssh_key {
      key_data = "<ssh-public-key>"
    }
  }

  os_profile_linux_config {
    disable_password_authentication = true
  }

  boot_diagnostics {
    enabled     = true
    storage_uri = "<storage-account-uri>"
  }
}

# Resource block for the second virtual machine
resource "azurerm_virtual_machine" "example_vm_2" {
  name                  = "example-vm-2"
  resource_group_name   = "<resource-group-name>"
  location              = "<azure-region>"
  availability_set_id   = "<availability-set-id>" # Optional, if using an availability set

  vm_size               = "Standard_DS2_v2"
  network_interface_ids = ["<network-interface-id>"]

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "example-vm-2"
    admin_username = "adminuser"
    ssh_key {
      key_data = "<ssh-public-key>"
    }
  }

  os_profile_linux_config {
    disable_password_authentication = true
  }

  boot_diagnostics {
    enabled     = true
    storage_uri = "<storage-account-uri>"
  }
}
