# modules/key_vault/variables.tf

variable "key_vault_name" {
  description = "Name of the Azure Key Vault"
}

variable "location" {
  description = "Azure Region"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "tenant_id" {
  description = "Azure AD Tenant ID"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Key Vault"
}
