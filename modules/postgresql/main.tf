# modules/postgresql/main.tf

resource "azurerm_postgresql_server" "postgresql" {
  name                = var.postgresql_server_name
  resource_group_name = var.resource_group_name
  location            = var.location
  version             = "11"

  administrator_login          = var.admin_username
  administrator_login_password = var.admin_password

  sku {
    name     = "B_Gen5_1"
    tier     = "Basic"
    capacity = 50
  }

  storage_profile {
    storage_mb            = 5120
    backup_retention_days = 7
    geo_redundant_backup  = "Disabled"
  }

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
