# modules/postgresql/variables.tf

variable "postgresql_server_name" {
  description = "Name of the PostgreSQL Server"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "admin_username" {
  description = "Administrator username for the PostgreSQL Server"
}

variable "admin_password" {
  description = "Administrator password for the PostgreSQL Server"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the PostgreSQL Server"
}
