# modules/aks/variables.tf

variable "aks_cluster_name" {
  description = "Name of the AKS Cluster"
}

variable "location" {
  description = "Azure Region"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "client_id" {
  description = "Azure AD Client ID"
}

variable "client_secret" {
  description = "Azure AD Client Secret"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the AKS Cluster"
}
