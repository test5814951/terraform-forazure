# modules/virtual_network/main.tf

resource "azurerm_virtual_network" "virtual_network" {
  name                = var.virtual_network_name
  resource_group_name = var.resource_group_name
  location            = var.location
  address_space       = var.address_space

  subnet {
    name           = var.subnet_name
    address_prefix = var.subnet_address_space[0]
  }

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
