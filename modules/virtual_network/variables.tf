# modules/virtual_network/variables.tf

variable "virtual_network_name" {
  description = "Name of the Virtual Network"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "address_space" {
  description = "Address space for the Virtual Network"
  type        = list(string)
}

variable "subnet_name" {
  description = "Name of the subnet within the Virtual Network"
}

variable "subnet_address_space" {
  description = "Address space for the subnet"
  type        = list(string)
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Virtual Network"
}
