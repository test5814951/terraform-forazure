# modules/storage_account/variables.tf

variable "storage_account_name" {
  description = "Name of the Storage Account"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Storage Account"
}
