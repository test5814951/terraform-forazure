# modules/load_balancer/variables.tf

variable "load_balancer_name" {
  description = "Name of the Load Balancer"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "public_ip_address_id" {
  description = "ID of the public IP address for the Load Balancer"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Load Balancer"
}
