# modules/load_balancer/main.tf

resource "azurerm_lb" "load_balancer" {
  name                = var.load_balancer_name
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "LoadBalancerFrontend"
    public_ip_address_id = var.public_ip_address_id
  }

  backend_address_pool {
    name = "LoadBalancerBackendPool"
  }

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
