# modules/cdn/main.tf

resource "azurerm_cdn_profile" "cdn" {
  name                = var.cdn_profile_name
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Standard_Verizon"

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
