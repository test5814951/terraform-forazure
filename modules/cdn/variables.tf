# modules/cdn/variables.tf

variable "cdn_profile_name" {
  description = "Name of the CDN Profile"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the CDN Profile"
}
