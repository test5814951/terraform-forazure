# modules/app_gateway/main.tf

resource "azurerm_application_gateway" "app_gateway" {
  name                = var.app_gateway_name
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Standard_Small"
  capacity            = 2

  gateway_ip_configuration {
    name      = "appGatewayIpConfig"
    subnet_id = var.subnet_id
  }

  frontend_port {
    name = "appGatewayFrontendPort"
    port = 80
  }

  frontend_ip_configuration {
    name                 = "appGatewayFrontendIP"
    public_ip_address_id = var.public_ip_address_id
  }

  backend_address_pool {
    name = "appGatewayBackendPool"
  }

  backend_http_settings {
    name                  = "appGatewayBackendHttpSettings"
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
  }

  http_listener {
    name                           = "appGatewayHttpListener"
    frontend_ip_configuration_name = "appGatewayFrontendIP"
    frontend_port_name             = "appGatewayFrontendPort"
  }

  request_routing_rule {
    name                       = "appGatewayRule"
    rule_type                  = "Basic"
    http_listener_name         = azurerm_application_gateway.http_listener.name
    backend_address_pool_name  = azurerm_application_gateway.backend_address_pool.name
    backend_http_settings_name = azurerm_application_gateway.backend_http_settings.name
  }

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
