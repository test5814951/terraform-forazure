# modules/app_gateway/variables.tf

variable "app_gateway_name" {
  description = "Name of the Application Gateway"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "subnet_id" {
  description = "ID of the subnet to deploy the Application Gateway"
}

variable "public_ip_address_id" {
  description = "ID of the public IP address for the Application Gateway"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Application Gateway"
}
