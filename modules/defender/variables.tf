# modules/defender/variables.tf

variable "defender_plan_name" {
  description = "Name of the Microsoft Defender plan"
}

variable "resource_group_name" {
  description = "Name of the Azure Resource Group"
}

variable "location" {
  description = "Azure Region"
}

variable "environment" {
  description = "Environment (e.g., dev, uat, prod)"
}

variable "owner" {
  description = "Owner of the Microsoft Defender plan"
}
