# modules/defender/main.tf

resource "azurerm_security_center_subscription_pricing" "defender" {
  name                = var.defender_plan_name
  resource_group_name = var.resource_group_name
  tier                = "Standard"

  tags = {
    environment = var.environment
    owner       = var.owner
  }
}
