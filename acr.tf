resource "azurerm_container_registry" "example_acr" {
  name                     = "exampleacr"
  resource_group_name      = "<resource-group-name>"
  location                 = "<azure-region>"
  sku                      = "Standard"
  admin_enabled            = true
  georeplication_locations = ["<georeplication-location-1>", "<georeplication-location-2>"]

  network_rule_set {
    default_action = "Deny"
    virtual_network_subnet_ids = [
      "<subnet-id-1>",
      "<subnet-id-2>",
    ]
  }

  admin_user_enabled = true
}
