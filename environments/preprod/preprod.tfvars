# preprod.tfvars

# Azure AD
tenant_id       = "your-preprod-tenant-id"
client_id       = "your-preprod-client-id"
client_secret   = "your-preprod-client-secret"
subscription_id = "your-preprod-subscription-id"

# Common
location              = "East US"
resource_group_name   = "preprod-rg"
virtual_network_name  = "preprod-vnet"
subnet_name           = "preprod-subnet"
admin_username        = "adminuser"
admin_password        = "PreProdAdminPassword123!"  # Use secure methods for handling passwords in production

# Key Vault
key_vault_name = "preprod-keyvault"

# AKS
aks_cluster_name = "preprod-aks"

# CDN
cdn_profile_name = "preprod-cdn"

# Application Gateway
app_gateway_name = "preprod-appgw"

# Virtual Network
vnet_address_space        = ["10.3.0.0/16"]
subnet_address_space      = ["10.3.1.0/24"]

# Storage Account
storage_account_name = "preprodstorage"

# PostgreSQL
postgresql_server_name = "preprod-postgres"

# Load Balancer
load_balancer_name = "preprod-lb"

# Microsoft Defender
defender_plan_name = "preprod-defender"

# Reference the shared variables
shared_vars_file = "shared.tfvars"
