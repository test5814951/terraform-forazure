# dev.tfvars

# Azure AD
tenant_id       = "your-dev-tenant-id"
client_id       = "your-dev-client-id"
client_secret   = "your-dev-client-secret"
subscription_id = "your-dev-subscription-id"

# Common
location              = "East US"
resource_group_name   = "dev-rg"
virtual_network_name  = "dev-vnet"
subnet_name           = "dev-subnet"
admin_username        = "adminuser"
admin_password        = "DevAdminPassword123!"  # Use secure methods for handling passwords in production

# Key Vault
key_vault_name = "dev-keyvault"

# AKS
aks_cluster_name = "dev-aks"

# CDN
cdn_profile_name = "dev-cdn"

# Application Gateway
app_gateway_name = "dev-appgw"

# Virtual Network
vnet_address_space        = ["10.1.0.0/16"]
subnet_address_space      = ["10.1.1.0/24"]

# Storage Account
storage_account_name = "devstorage"

# PostgreSQL
postgresql_server_name = "dev-postgres"

# Load Balancer
load_balancer_name = "dev-lb"

# Microsoft Defender
defender_plan_name = "dev-defender"

# Reference the shared variables
shared_vars_file = "shared.tfvars"
