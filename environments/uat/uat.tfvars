# uat.tfvars

# Azure AD
tenant_id       = "your-uat-tenant-id"
client_id       = "your-uat-client-id"
client_secret   = "your-uat-client-secret"
subscription_id = "your-uat-subscription-id"

# Common
location              = "East US"
resource_group_name   = "uat-rg"
virtual_network_name  = "uat-vnet"
subnet_name           = "uat-subnet"
admin_username        = "adminuser"
admin_password        = "UATAdminPassword123!"  # Use secure methods for handling passwords in production

# Key Vault
key_vault_name = "uat-keyvault"

# AKS
aks_cluster_name = "uat-aks"

# CDN
cdn_profile_name = "uat-cdn"

# Application Gateway
app_gateway_name = "uat-appgw"

# Virtual Network
vnet_address_space        = ["10.2.0.0/16"]
subnet_address_space      = ["10.2.1.0/24"]

# Storage Account
storage_account_name = "uatstorage"

# PostgreSQL
postgresql_server_name = "uat-postgres"

# Load Balancer
load_balancer_name = "uat-lb"

# Microsoft Defender
defender_plan_name = "uat-defender"

# Reference the shared variables
shared_vars_file = "shared.tfvars"
