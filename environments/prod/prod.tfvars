# prod.tfvars

# Azure AD
tenant_id       = "your-prod-tenant-id"
client_id       = "your-prod-client-id"
client_secret   = "your-prod-client-secret"
subscription_id = "your-prod-subscription-id"

# Common
location              = "East US"
resource_group_name   = "prod-rg"
virtual_network_name  = "prod-vnet"
subnet_name           = "prod-subnet"
admin_username        = "adminuser"
admin_password        = "ProdAdminPassword123!"  # Use secure methods for handling passwords in production

# Key Vault
key_vault_name = "prod-keyvault"

# AKS
aks_cluster_name = "prod-aks"

# CDN
cdn_profile_name = "prod-cdn"

# Application Gateway
app_gateway_name = "prod-appgw"

# Virtual Network
vnet_address_space        = ["10.4.0.0/16"]
subnet_address_space      = ["10.4.1.0/24"]

# Storage Account
storage_account_name = "prodstorage"

# PostgreSQL
postgresql_server_name = "prod-postgres"

# Load Balancer
load_balancer_name = "prod-lb"

# Microsoft Defender
defender_plan_name = "prod-defender"

# Reference the shared variables
shared_vars_file = "shared.tfvars"
