terraform {
    lock {
    lock_info {
      path    = "terraform.tflock"
      timeout = "20m"
    }
  }
  backend "azurerm" {
    resource_group_name   = "<your-backend-resource-group>"
    storage_account_name   = "<your-backend-storage-account>"
    container_name         = "<your-backend-container>"
    key                    = "terraform.tfstate"
    subscription_id        = "<your-subscription-id>"
    tenant_id              = "<your-tenant-id>"
    client_id              = "<your-client-id>"
    client_secret          = "<your-client-secret>"
  }
}
